

%\usetheme{Warsaw}  % <-- I like the TOC at the top of each slide.


\title{Divide \& Conquer (CLRS 4)}
\subtitle{SER501 - Lecture Notes}
\author{John C. Femiani}


%% I was asked to include slide numbers so that students can 
%% reference slides in their notes.
%\setbeamertemplate{footline}[frame number]  %<-- That replaces the whole footer, no good
\newcommand*\oldmacro{}%
\let\oldmacro\insertshorttitle%
\renewcommand*\insertshorttitle{%
	\oldmacro\hfill%
	\insertframenumber\,/\,\inserttotalframenumber}
%% End of adding page number

\setbeamercovered{transparent}

\usepackage[procnames]{listings}
\usepackage{color}
\usepackage{clrscode}
\usepackage{cancel}
\usepackage{graphicx}


\definecolor{keywords}{RGB}{255,0,90}
\definecolor{comments}{RGB}{0,0,113}
\definecolor{red}{RGB}{160,0,0}
\definecolor{green}{RGB}{0,150,0}
\lstset{language=Python, 
	basicstyle=\ttfamily\small, 
	keywordstyle=\color{keywords},
	commentstyle=\color{comments},
	stringstyle=\color{red},
	showstringspaces=false,
	identifierstyle=\color{green},
	procnamekeys={def,class},
	numbers=left,
	showspaces=false}



\begin{document}
	
\begin{frame}
	\titlepage	
\end{frame}



\begin{frame} 
  	\frametitle{Outline}
  	\tableofcontents
\end{frame}
   
   
\lecture{Week 2, Lecture 1}{w2l1}
   
\begin{frame}
	\frametitle{Overview}
	\framesubtitle{What is Divide \& Conquer}
	Recall the D\&C strategy from our \textsc{Merge-Sort} example. 
	
	\begin{description}
		\item[Divide] the problem into a number of subproblems that are \alert{smaller} instances of the \alert{same} problem.
		\item[Conquer] the subproblems by solving them recursively. \\
		\alert{Base case:} if subproblems are small, solve them by brute force.
		\item[Combine] the subproblem solutions to give a solution to the original problem.
	\end{description}
	
	This chapter covers two more algorithms based in D\&C and provides you with the mathematical tools to analyze (or predict) the complexity of D\&C algorithms.
\end{frame}   

\begin{frame}
	\frametitle{Overview}
	\framesubtitle{Analyzing divide and conquer}

	We use a \alert{recurrence} to characterize the running time of divide-and-conquer algorithms. 
	
	\begin{definition}[Recurrence]
		A \alert{recurrence} is a function defined in terms of
		\begin{itemize}
			\item on or more base cases, and
			\item itself, with smaller arguments.
		\end{itemize}
	\end{definition}
	
	\begin{exampleblock}{Example}
		$$T(n) = \begin{cases}
		1 & \text{if $n=1$}\\
		T(n-1) +  1 & \text{if $n>1$}.
		\end{cases}$$
		
		Solution: $T(n) = n$
	\end{exampleblock}

\end{frame}


\begin{frame}[shrink=20]
	\frametitle{Overview}
	\framesubtitle{Analyzing divide and conquer}
	
	\begin{exampleblock}{Example}
		$$T(n) = \begin{cases}
		1 & \text{if $n=1$}\\
		2 T(n/2) +  n & \text{if $n \geq 1$}.
		\end{cases}$$
		
		Solution: $T(n) = n \lg n + n$
	\end{exampleblock}
	
	\begin{exampleblock}{Example}
		$$T(n) = \begin{cases}
		1 & \text{if $n=2$}\\
		T(\sqrt{n}) +  1 & \text{if $n > 2$}.
		\end{cases}$$
		
		Solution: $T(n) = \lg \lg n$
	\end{exampleblock}	
	
	\begin{exampleblock}{Example}
		$$T(n) = \begin{cases}
		1 & \text{if $n=1$}\\
		T(n/3) + 2T(2n/3) + n & \text{if $n > 1$}.
		\end{cases}$$
		
		Solution: $T(n) = \Theta(n lg n)$
	\end{exampleblock}
	
\end{frame}
   
\begin{frame}
	There are many technical issues:
	\begin{itemize}[]
		\item Floors and ceilings.  Note that these don't effect the solution of the recurrence and they are better left to a discrete math course. 
		\item Exact vs. asymptotic functions.
		\item boundary conditions. 
	\end{itemize}
	
	\begin{block}{Note}
	  In algorithm analysis, we usually express both a recurrence and its solution using asymptotic notation.
	\end{block}
	
	\begin{itemize}[]
		\item Example: $T(n) = 2 T(n/2) + \Theta(n)$, with solution $T(n) = \Theta(n \lg n)$. 
		\item The boundary conditions are ``$T(n) = O(1)$ for sufficiently small $n$,'' and are not generally stated unless we desire exact solutions.
	\end{itemize}
\end{frame}
   
\section{Two divide and conquer algorithms}
\subsection{The maximum sub-array problem}

\begin{frame}
	\frametitle{The max-subarray problem}
	\subtitle{Scenario}
	\begin{itemize}[]
		\item You have the prices of a stock traded over a period of $n$ consecutive days.
		\item When should you have (in retrospect) bought the stock? When should you have sold?
		\item Even though it is in retrospect, you can use this as a basis for yelling at your stock broker for not recommending those buy and sell dates. 
	\end{itemize}
	\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{"IM/Chapter 4/Fig-4-1"}
\caption{}
\label{fig:Fig-4-1}
\end{figure}

\end{frame}


\begin{frame}
	We cast the problem as:
	\begin{description}[]
		\item[Input:] An array $A[1..n]$ of numbers.
		\item[Output:] Indices $i$ and $j$ such that $A[i..j]$ has the greatest sum of any \alert{non-empty}, \alert{contigous} subarray of $A$, along with the sum of the values in $A[i..j]$. 
	\end{description}
	
	\begin{itemize}[]
		\item Unless some $A[i]$ are  negative,  the solution is trivial.
		\item $A[i] = (\text{price after day } i)-(\text{price after day } (i-1))$. \alert{Why}?
		\item If the solution is $A[i...j]$, you should have bought just \emph{before} day $i$ and sold just \emph{after} day $j$.
	\end{itemize}

\end{frame}

\begin{frame}
	Why not just buy low, sell high?
	\begin{itemize}
		\item Lowest price might occur \alert{after} the highest price.
		\item Optimal strategy might not involve buying high \emph{or} selling low.
	\end{itemize}
	\begin{center}
\includegraphics[width=0.7\linewidth]{"IM/Chapter 4/Fig-4-2"}
\end{center}
The maximum profit is $\$3$ per share, from buying just after day 2 and selling after day 3. 
What is the low price? the high price? and what are $i$ and $j$?

\end{frame}

\begin{frame}
	\frametitle{Brute Force}
	You can solve by brute force:
	\begin{itemize}
		\item Check all $\binom{n}{2} = \Theta(n^2)$ subarrays. \alert{How?}
		\item Can organize the computation so that each subarray $A[i..j]$ takes $O(1)$ time, given that you already have computed $A[i, j-1]$.
		\item Brute force can be done in $O(n^2)$ time.
	\end{itemize}
	
	\begin{block}{Note}
		It is often a good idea to start solving a problem by considering what brute force would be. In this case B.F is tractible, but we can do better.  Often B.F. is a way to test or better understand the problem.
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Solving by divide and conquer}
	\alert{Notation for subproblems:} Find a max subarray of $A[low..high]$, in original problem $low=0$ and $high=n$.
	
	\begin{description}
		\item[Divide] the subarray into two subarray as equal in size as possible. Find the mid-point and consider $A[low..mid]$ and $A[mid+1..high]$.
		\item[Conquer] by finding the max subarray of $A[low..mid]$ and $A[mid+1 .. high]$.
		\item[Combine] by choosing one of:
		\begin{itemize}
			\item The max subarray of $A[low..mid]$
			\item The max subarray of $A[mid+1..high]$
			\item A subarray that crosses the midpoint (this is an easier problem)
		\end{itemize}
	\end{description}
\end{frame}

\begin{frame}
	\frametitle{Finding the max subarray that crosses the midpoint}
	This is \alert{not} a smaller instance of the original problem: there is an added restriction that the solution must cross the midpoint.
	
	\begin{itemize}
		\item Any solution crossing the mid-point is made of two subarrays, $A[i..mid]$ and $A[mid+1..j]$ where $low \leq i \leq j$ and $mid < j \leq high$. 
		\item Each of those should be the largest SA that includes $mid$.
		\item Each involves only $Theta(n)$ possible solutions...
	\end{itemize}
	
\end{frame}

\begin{frame}

\centering
\includegraphics[width=0.7\linewidth]{IM/PSEUDOCODE/Find-Max-Crossing-Subarray}

\end{frame}

\begin{frame}
	\alert{Time:} the two loops together each process each index in $low..high$ exactly once, and do $\Theta(1)$ work.  Therefore the procedure takes $\Theta(n)$ time. 
\end{frame}

\begin{frame}
	\frametitle{D\&C procedure for the max-subarray problem}
	\begin{center}
\includegraphics[width=0.9\linewidth]{IM/PSEUDOCODE/Find-Maximum-Subarray}
\end{center}
\alert{Initial call:} \textsc{Find-Maximum-Subarray}$(A, 1, n)$
\end{frame}

\begin{frame}
	\begin{itemize}
		\item Divide by computing $mid$. 
		\item Conquer by two recursive calls to \textsc{Find-Maximum-Subarray}.
		\begin{itemize}
			\item Base case is when the subarray has only one element. Solution is BF.
		\end{itemize}
		\item Combine by calling \textsc{Find-Max-Crossing-Subarray} and then determining which of the three results has the maximum sum.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Analysis}
	\alert{Simplifying assumption:} The original problem is a power of 2, so all subproblem sizes are integers. (Why is this ok?)
	
	
	\alert{Notation:} Let $T(n)$ denote the time on a subarray of $n$ elements. 
	
	\alert{Base case:} if $high==low$ it does $\Theta(1)$ work.\\
	
	\alert{Recursive case:}
	\begin{itemize}[]
		\item Conquering solves two subproblems, each on a subarray of $n/2$  $\implies 2T(n/2)$ for conquering.
		\item Combining consists of calling \textsc{Find-Max-Crossing-Subarray} which is $\Theta(n)$ and doing $\Theta(1)$ tests $\implies \Theta(n) + \Theta(1)$.
	\end{itemize}
	{
	Recurrence is:
	\begin{align*}
		T(n) &= \underbrace{\Theta(1)}_{Divide} + \underbrace{2T(n/2)}_{Conquer} + \underbrace{\Theta(n) + \Theta(1)}_{Combine} \\
		&= 2T(n/2) + \Theta(n)
	\end{align*}
}
\end{frame}

\begin{frame}
	\frametitle{Analysis}
	The recurrence is 
	$$
	T(n) = \begin{cases}
	\Theta(1) & \text{if $n=1$,}\\
	2 T(n/2) + \Theta(n) & \text{if $n > 1$.}
	\end{cases}$$
	
	\begin{itemize}
		\item This is a familiar recurrence...
		\item The solution is $T(n) = \Theta(n \lg n)$
		\item It is an improvement over $\Theta(n^2)$ for brute force. 
		\item But it is still not optimal -- exercise CLRS 4.1-5
	\end{itemize}
\end{frame}

\lecture{Week 2, Lecture 2}{w2l2}

\begin{frame}
	\frametitle{What to take away}
	\framesubtitle{from the max-subarray problem}
	\begin{itemize}
		\item It is good to consider what a brute force solution would be, to help understand the size and nature of the solution space.
		\item Dividing a problem into subproblems \alert{can}  result substantial time savings.
		\item It is not always obvious how to combine solutions efficiently - if we had not found a $\Theta(n)$ solution for \textsc{Find-Max-Crossing-Subarray} our analysis would have been quite different.
		\item If memory serves, the optimal solution to this problem is actually incremental. $D\&C$ is not \alert{always} a magic bullet.
	\end{itemize}
\end{frame}

\subsection{Strassen's algorithm for matrix multiplication}
\begin{frame}
	\frametitle{Square Matrix Multiplication}
	\begin{itemize}
		\item One things computers are great for is multiplying large matrices.
		\item If you aren't already familiar enough with matrix multiplication, read CLSR-Appendix D right away.
		\item Matrices are important in computer graphics, and hugely important in finite element analysis, engineering, structural design simulations, image processing, .... 
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Square Matrix Multiplication}
	\framesubtitle{Problem}
	\begin{description}
		\item[Input:] Two $n \times n$ (square) matrices,\\ $A=(a_{ij})$ and $B=(b_{ij})$.
		\item[Output:] One $n \times n$ matrix $C=(c_{ij})$ where $C=A\cdot B$, i.e.,
		$$c_{ij} = \sum_{k=1}^n a_{ik}b_{kj}$$
		for $i,j = 1, 2, .., n.$
	\end{description}
	{
	\begin{center}
	\includegraphics[width=0.7\linewidth]{square-matrix-multiply}
	\end{center}
	}

\end{frame}

\begin{frame}[fragile]
	\frametitle{Obvious Method}
	
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{exampleblock}{pseudocode}
			\begin{center}
			\includegraphics[width=0.9\linewidth]{IM/PSEUDOCODE/Square-Mat-Mult}
			\end{center}
			\end{exampleblock}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{exampleblock}{Python}
			\begin{lstlisting}[ basicstyle=\ttfamily\tiny]
def square_mat_mult(A, B, n):
    C = numpy.empty((n,n))
    for i in range(n):
        for j in range(n):
            C[i,j] = 0
            for k in range(n):
                C[i,k] += A[i,k]*B[i,k]
    return C\end{lstlisting}
			{\tiny	
			or just \texttt{C = A.dot(B)} since \texttt{numpy} knows matrices.}
			\end{exampleblock}
		\end{column}
	\end{columns}
	\vfill
	\alert{Analysis:} Three nested loops, each iterates $n$ times, and the innermost loop takse $\Theta(1)$ $\implies \Theta(n^3)$.
\end{frame}

\begin{frame}
	\frametitle{But is $\Theta(n^3)$ the best we can do?}
	\framesubtitle{Can we multiply matrices in $o(n^3)$ time?}
	It \emph{looks} like any algorithm must be $\Omega(n^3)$ time:
	\begin{itemize}
		\item There are $n^2$ entries
		\item Each adds $n$ terms
	\end{itemize}
	But \alert{Strassen's method} solves it in $o(n^3)$ time.
	\begin{itemize}
		\item Strassen's algorithm is $\Theta(n^{\lg 7})$ time.
		\item $2.8 \leq \lg 7 \leq 2.81$,
		\item so it is $O(n^{2.81})$ and $\Omega(n^{2.8})$
	\end{itemize}
	
\end{frame}

\begin{frame}
	\frametitle{Simple D\&C approach (won't work yet)}
	
	Partition each matrix into four $n/2 \times n/2$ matrices. 
	
	$$
	A = \left(\begin{matrix} A_{11} & A_{12} \\  A_{21} & A_{22} \end{matrix}\right), 
	B = \left(\begin{matrix} B_{11} & B_{12} \\  B_{21} & B_{22} \end{matrix}\right),
	C = \left(\begin{matrix} C_{11} & C_{12} \\  C_{21} & C_{22} \end{matrix}\right).
	$$
	
	\begin{block}{Note 1}
		\alert{Please, review CLRS-D if this looks unfamiliar to you}. It should not be completely new to you. 
	\end{block}
	\begin{block}{Note 2}
		This can be a confusing part of D\&C -- how can one break the problem up? There are often multiple ways. 
	\end{block}
\end{frame}
\begin{frame}
	We can rewrite matrix multiplication as:
	$$
	\left(\begin{matrix}
	 \uncover<1,2,6->{C_{11}} &
	 \uncover<1,3,6->{C_{12}} \\  
	 \uncover<1,4,6->{C_{21}} & 
	 \uncover<1,5,6->{C_{22}} \end{matrix}\right) = 
	 \left(\begin{matrix} 
	 \uncover<1,2,3,6->{A_{11}} & 
	 \uncover<1,2,3,6->{A_{12}} \\ 
	 \uncover<1,4,5,6->{A_{21}} & 
	 \uncover<1,4,5,6->{A_{22}} 
	 \end{matrix}\right) 
	 \cdot 
	 \left(\begin{matrix} 
	 \uncover<1,2,4,6->{B_{11}} & 
	 \uncover<1,3,5,6->{B_{12}} \\ 
	 \uncover<1,2,4,6->{B_{21}} & 
	 \uncover<1,3,5,6->{B_{22}} 
	  \end{matrix}\right).
	$$
	giving four equations:
	\begin{align*}
	\uncover{C_{11} &= A_{11} \cdot B_{11} + A_{12} \cdot B_{21},\\ }
	\uncover{C_{12} &= A_{11} \cdot B_{12} + A_{12} \cdot B_{22},\\ }
	\uncover{C_{21} &= A_{21} \cdot B_{11} + A_{22} \cdot B_{21},\\ }
	\uncover{C_{22} &= A_{21} \cdot B_{12} + A_{22} \cdot B_{22},\\ }
	\end{align*}
	\uncover{Each of those equations multiplies two $n/2 \times n/2$ matrices and then adds their $n/2 \times n/2$ products.}
\end{frame}

\begin{frame}[fragile]
We can use these equations for a recursive D\&C algorithm \\ (with how many subproblems? How big?):

\begin{center}
\includegraphics[width=1.0\linewidth]{IM/PSEUDOCODE/Rec-Mat-Mult}
\end{center}

\begin{block}{Note}
    You do not need to copy in order to pass (or return) sub-matrices.
    \begin{itemize}
        \item In python, you can use slicing to create lightweight views.
    \end{itemize}
\end{block}

\end{frame}

\begin{frame}
    \frametitle{Analysis}
    Let $T(n)$ be the time to multiply two $n/2 \times n/2$ matrices.
    
    \alert{Base case:} $n-1.$ Perform one scalar multiplication in $\Theta(1)$.
    \alert{Recursive case:} $n>1$.
    \begin{itemize}
        \item \alert{Dividing} takes $\Theta(1)$ time using slicing. 
        \item \alert{Conquering} makes 8 recursive calls each multiplying $n/2 \times n/2$ matrices $\implies 8T(n/2)$.
        \item \alert{Combining} takes $\Theta(n^2)$ time to add $n/2 \times n/2$ matrices four times.
    \end{itemize}
    $$T(n) = \begin{cases}
    \Theta(1) & \text{if $n=1$}\\
    \underbrace{8 T(n/2)}_{Conquer} + \underbrace{\Theta(n^2)}_{Divide \& Combine} & \text{if $n>1$}
    \end{cases}$$
\end{frame}
\begin{frame}
    \frametitle{Analysis}
    \begin{itemize}
        \item Unlike the other recurrence, we solve $8$ subproblems at each level of a recursion tree. 
        \item The height of the tree is still $\lg n$, but each level is $8$ times as many nodes as the previous one 
        \item Consider the last level when $n=2^k$ ...
        \begin{itemize}
            \item $8^{\lg n} = n^{\lg 8} = n^3$ leaf nodes in the recursion tree. 
            \item Recall the logarithmic identities ...
        \end{itemize}
        \item This solution is probably $\Omega(n^3)$  
        \item The issue is $8$ sub-problems, if there were fewer subproblems there would be fewer leaves inthe recursion tree \\(e.g 7 subproblems $\implies n^{\lg 7} leaf nodes$)
    \end{itemize}
    
\end{frame}
\begin{frame}
    \frametitle{Strassen's method}
    \begin{itemize}
        \item The key is to divide into fewer sub-problems.
        \item We can do a lot more additions\\
         (they get absorbed in the $\Theta(n^2)$ cost of combining)\\
          if it means fewer sub-problems.
        \item Strassen found a way to do it in $7$ multiplications 
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Strassen's method}
    \begin{enumerate}[]
        \item Partition matrices as before: $\Theta(1)$.
        \item Now we create $10$ matrrices, $S_1, S_2, .., S_{10}$. Each is a sum or difference of a matrix from the previous step.  Time is $\Theta(n^2)$ to create all 10.
        
        \item Recursively compute 7 matrix products involving the 10 matrices.  $P_1, P_2, .., P_7$ each $n/2 \times n/2$. 
        \item Compute the $n/2 \times n/2$ submatrices of $C$ by adding and subtracting the $P_i$, time is $\Theta(n^2)$.
    \end{enumerate}
    \uncover{
    \alert{Analysis:}  The solution is now $\Theta(n^{\lg 7})$, but the next part of this lecture will give us the tools to prove that. }
\end{frame}

\begin{frame}
    \frametitle{Gratuitous details}
    \framesubtitle{These are in the book}
    \alert{Step 2:} The 10 matrices are:
    \begin{tabular}{lll}
             $S_1 = B_{12} - B_{22},$ &
             $S_2 = A_{11} + A_{12},$ &
             $S_3 = A_{21} + A_{22},$ \\
             $S_4 = B_{21} - B_{11},$ &
             $S_5 = A_{11} + A_{22},$ &
             $S_6 = B_{11} + B_{22},$ \\
             $S_7 = A_{12} - A_{22},$ &
             $S_8 = B_{21} + B_{22},$ &
             $S_9 = A_{11} - A_{21},$ \\            
             $S_{10} = B_{11} + B_{12}$ &
             &
    \end{tabular}
   \vfill    
   \alert{Step 3:} The products are:
   \begin{tabular}{llll}
       $P_1 = A_{11} S_1$, & $P_2 = S_2 B_{22}$, &$P_3 = S_3 B_{11}$,& $P_4 = A_{22} S_4$, \\
       $P_5 = S_5 S_6$, & $P_6 = S_7 S_8$,& $P_7=S_9 S_{10}$&
   \end{tabular}
   \vfill    
   
   \alert{Step 4:} The submatrices of $C$ are:
    \begin{tabular}{ll}
    $C_{11} = P_5 + P_4 - P_2 + P_6$, & $C_{12} = P_1 + P_2$ \\
    $C_{21} = P_3 + P_4$, & $C_{22} = P_5 + P_1 - P_3 - P_7$
    \end{tabular}   
   
\end{frame}

\begin{frame}[shrink=20]
\frametitle{Let's try one of those to see how it works...}
\vspace{0.5cm}
For $C_{11} = P_5 + P_4 - P_2 + P_6$:

\begin{align*}
 \uncover{P_5 &= S_5 S_6 &&=(A_{11} + A_{22})(B_{11}+B_{22}) &&= A_{11}B_{11} + A_{11}B_{22} + A_{22}B_{11}+ A_{22}B_{22}\\}
 \uncover{P_4 &= A_{22} S_4 &&= A_{22}(B_{21} - B_{11}) &&= A_{22}B_{21} -A_{22}B_{11}\\}
 \uncover{P_2 &= S_2 B_{22} &&= (A_{11} + A_{12})B_{22} &&= A_{11}B_{22} +A_{12}B_{22}\\}
 \uncover{P_6 &= S_7 S_8 &&= (A_{12}- A_{22})(B_{21} + B_{22}) &&= A_{12} B_{21} + A_{12}B_{22} -A_{22}B_{21} - A_{22}B_{22}}
\end{align*}
\vfill
\begin{tabular}{r|lllllll}
    $P_5$  & \uncover{ $A_{11}B_{11}$ & $+A_{11}B_{22}$ & $+A_{22}B_{11}$ & $+A_{22}B_{22}$ \\}
    $+P_4$ & \uncover{              &                 & $-A_{22}B_{11}$ &                 & $+A_{22}B_{11}$\\}
    $-P_2$ & \uncover{              & $-A_{11}B_{22}$ &                 &                 &                 & $-A_{12}B_{22}$\\}
    $+P_6$ & \uncover{              &                 &                 & $-A_{22}B_{22}$ & $-A_{22}B_{11}$ & $+A_{12}B_{22}$ & $+A_{12}B_{21}$}\\
    \hline
    $C_{11}=$& \uncover{ $A_{11}B_{11}$ &              &                  &                  &                &                 &$+A_{12}B_{21}$}
  \end{tabular}  
  
  \vspace{0.5cm}
 \uncover{\alert{Come on Strassen, this a piece of cake!}}
\end{frame}

\begin{frame}
    \frametitle{Thoeretical \& practical issues}
    \begin{itemize}
    \item First to be $o(n^3)$, but not the current champion.  A method is $O(n^{2.376})$
    \item Higher constants than the $\Theta(n^3)$ approach, crossover points as low as $n=400$. 
    \item Not good for sparse matrices (which many matrices are).
    \item Numerical issues -- larger rounding errors accumulate. (Not as bad in practice as people initially thought)
    \item The submatrices of $C$ consume space. 
    \end{itemize}
\end{frame}

\section{Analysis of D\&C}

\subsection{The substitution method for solving recurrences}
\begin{frame}
    \frametitle{Substutition Method}
    So how can we solve recurrences?
    \begin{enumerate}
        \item Guess general form of the solution
        \item Use induction to find constants, show that solution works.
    \end{enumerate} 
\end{frame}


\begin{frame}[shrink=5]
    \frametitle{Example}
    $$T(n) = \begin{cases}
    1 & \text{if $n=1$,}\\
    2 T(n/2) + n & \text{if $n > 1$.}
    \end{cases}$$
\begin{itemize}[]
    \item \emph{Guess:} $T(n) = n \lg n + n$. How did we guess that?
    \item \alert{Basis:} $n=1 \implies n \lg n + n = 1 = T(n)$\\
    \item \alert{Inductive step:} 
    \begin{align*}
    \uncover{T(n) &= 2T(n/2) + n \\}
    \uncover{&= 2\left(\frac{n}{2}\lg \frac{n}{2} + \frac{n}{2}\right) + n & \text{(by ind. hyp.)} \\}
    \uncover{&= n \lg \frac{n}{2} +n + n & \text{distributing the 2}\\}
    \uncover{&= n(\lg n - \lg 2) + n + n & \text{a logarithmic identity}\\}
    \uncover{&= n\lg n -n + n + n & \text{since $\lg 2=1$, distribute $n$}\\}
    \uncover{&= n \lg n + n}
    \end{align*}
\end{itemize}
\end{frame}

\begin{frame}
    Generally, we use \alert{asymptotic notation}:
    \begin{itemize}[]
        \item We write $T(n) = 2T(n/2) + \Theta(n)$, no need for a base case.
        \item Assume $T(n) = O(1)$ for sufficiently small $n$.
        \item Express the solution in asymptotic notation: $T(n) = \Theta(n\lg n)$. 
        \item Ignore boundary \& base case in proofs:
        \begin{itemize}
            \item $T(n)$ constant for constant $n$.
            \item For asymptotic solution, there always exists a base case that works. 
            \item For exact solutions, we \emph{do} need a base case.
        \end{itemize}
        \item Prove upper ($O$) and lower ($\Omega$) bounds separately. 
        \item Replace `$=O(g(n))$' by `$\leq c g(n)$', but name the constants each time.  
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example (upper bound)}
    \begin{align*}
    T(n) = 2T(n/2) + \Theta(n) &\implies T(n) = 2T(n/2) + O(n) \\
         & \implies T(n) \leq 2T(n/2) + cn, &\text{ for some $c$.}
    \end{align*}
    
    \begin{itemize}
	\item All functions in $O(n)$ are $\leq cn$ for sufficiently large $n$
	\item We are given some constant $c$ from the $O$ definition -- we don't pick it. 
	\end{itemize}
       
\end{frame}

\begin{frame}[shrink=10]
    \frametitle{Upper bound ($O$)}
    \alert{Guess:} $T(n) \leq d n\lg n$ for some positive constant $d$.\\
     We pick $d$, this is for the right-hand-side of an equation.\\ 
     It is okay for $d$ to depend on $c$.\\ 
     \alert{Substitution:}
    \begin{align*}
    \uncover{T(n) &= 2T(n/2)+ \Theta(n) \\}
    \uncover{ &\leq 2T(n/2)+ c n & \text{(by defn. $\Theta$, $n\geq n_0$)} \\}
    \uncover{ &\leq 2\left(d \frac{n}{2} \lg \frac{n}{2}\right) + c n & \text{(by ind. hyp.)} \\}
    \uncover{ &= d n \lg \frac{n}{2} + c n \\}
    \uncover{ &= d n \lg n - d n + c n \\}
    \uncover{ &\leq d n \lg n & \text{if $-dn + cn $} & \leq 0,\\}
    \uncover{ & & d &\geq c}
    \end{align*}
    \uncover{Therefore $T(n) =O(n\lg n)$ ($n_0$ and $c$ inherited from $\Theta(n)$)}
\end{frame}

\begin{frame}
    \frametitle{Lower bound ($\Omega$)}
    \alert{Guess:} $T(n) \geq d n\lg n$ for some positive constant $d$.\\
     \alert{Substitution:}
    \begin{align*}
    \uncover{T(n) &= 2T(n/2)+ \Theta(n) \\}
    \uncover{ &\geq 2T(n/2)+ c n & \text{(by defn. $\Theta$, $n\geq n_0$)} \\}
    \uncover{ &\geq 2\left(d \frac{n}{2} \lg \frac{n}{2}\right) + c n & \text{(by ind. hyp.)} \\}
    \uncover{ &= d n \lg \frac{n}{2} + c n \\}
    \uncover{ &= d n \lg n - d n + c n \\}
    \uncover{ &\geq d n \lg n & \text{if $-dn + cn $} & \geq 0,\\}
    \uncover{ & & d &\leq c}
    \end{align*}
    \uncover{Therefore $T(n) =\Omega(n\lg n)$}
\end{frame}

\begin{frame}
    Make sure you show the \alert{exact} form you guessed in your proof. 
    $$T(n) = 8 T(n/2) + \Theta(n^2)$$
    $$T(n) \leq 8 T(n/2) + cn^2$$
    Guess: $T(n) \leq d n^3$.
  \begin{align*}
  \uncover{T(n) &\leq 8T(n/2)+ cn^2 \\}
  \uncover{ &\leq 8d(n/2)^3 + cn^2 \\}
  \uncover{ &= 8d(n^3/8) + cn^2 \\}
  \uncover{ &= dn^3 + cn^2 \\}
  \uncover{ &\not \leq dn^3 & \text{doesn't work! $c>0$}}
  \end{align*}
  \uncover{
  \alert{Remedy:} Make a more complicated guess -- subtract off a lower order term to try and cancel the $cn^2$.
  }
\end{frame}

\begin{frame}
    $$T(n) = 8 T(n/2) + \Theta(n^2)$$
    $$T(n) \leq 8 T(n/2) + cn^2$$
    Guess: $T(n) \leq d n^3 - d' n^2$.
    \begin{align*}
    \uncover{T(n) &\leq 8T(n/2)+ cn^2 \\}
    \uncover{ &\leq 8[d(n/2)^3 -d'(n/2)^2] + cn^2 \\}
    \uncover{ &= 8[d(n^3/8) - d'n^2/4] + cn^2 \\}
    \uncover{ &= dn^3 -2d'n^2 + cn^2 \\}
    \uncover{ &= dn^3 -d'n^2 -d'n^2 + cn^2 \\}
    \uncover{ &\leq dn^3 - d'n^2 & \text{if} -d'n^2 +cn^2 &\leq 0,}\\
     \uncover{  & & d' &\geq c}
    \end{align*}
   
\end{frame}

\begin{frame}
    \begin{alertblock}{Beware}
        Beware the false proof!
    \end{alertblock}
    
    \uncover{
    To prove $O$ or $\Omega$ you must \alert{guess} a function in $O$ or $\Omega$ and then prove the \alert{exact} form you guessed. 
    }
    \begin{exampleblock}{False proof}
        \begin{align*}
        T(n) & = 4 T(n/4) + n \\
        &\leq 4 [ c (n/4) ] + n & \text{(ind. hyp.)}\\
        &= cn + n \\
        &= O(n) & \text{wrong!}
        \end{align*} 
        You have to prove the \alert{exact} guess used in the inductive hypothesis, which was $T(n) \leq cn$. 
    \end{exampleblock}
\end{frame}

\subsection{The recursion tree method}
\begin{frame}
    \begin{block}{Note}
        In the lectures, I will not cover recursion trees in depth. 
        
        Please read the chapter and pay particular attention to the book-keeping done in the method:
        \begin{itemize}
            \item How does the size of the problem generally change as your level in the tree increases?
            \item How does the number of sub-problems changes?
            \item How many leaf nodes are there in the worste case?
            \item Notice that the worst of any of these costs (the cost per level, the cost at the root, the cost of the leaf nodes) becomes the cost of the recursion.
        \end{itemize}
    \end{block}
\end{frame}

\subsection{The master method for solving recurrences}
\begin{frame}
    \frametitle{The master method}
    Useful for D\&C recurrences of the form
    $$T(n) = a T(n/b) + f(n),$$
    where $a\geq 1, b > 1, f(n) > 0$. 
    
    \begin{itemize}
        \item $a$ is the number of sub-problems
        \item $n/b$ is the size of each sub-problem.
        \item $f(n)$ is the cost to divide and combine solutions. 
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{theorem}[4.1 - the Master theorem]
        Let $a \geq 1$ and $b > 1$ be constants, let $f(n)$ be a functions, and let $T(n)$ be defined over the nonnegative integers by the recurrence:
        $$T(n) = aT(n/b) + f(n),$$
        where $n/b$ may be interpreted at either $\lfloor n/b \rfloor$ or $\lceil n/b \rceil$. Then $T(n)$ has the following asymptotic bounds:
        \begin{enumerate}
            \item[1.] $f(n) = O(n^{\log_b a - \epsilon})$ for $\epsilon>0$ \hfill $\implies T(n) = \Theta(n^{\log_b a})$.
            \item[2.] $f(n) = \Theta(n^{\log_b a})$ \hfill $\implies T(n) = \Theta(n^{\log_b a} \lg n)$.
            \item[3.] $f(n) = \Omega(n^{\log_b a + \epsilon})$ for $\epsilon>0$ 
            \begin{itemize}
                \item and also $f(n)$ is \alert{regular} \hfill $\implies T(n) = \Theta(f(n))$.
            \end{itemize}
        \end{enumerate}
        
    \end{theorem}
    \alert{This is a lot to take in from a slide - but memorizing this will be helpful to you in this course.}
\end{frame}

\begin{frame}
    \frametitle{Understanding what the theorem says}
    \begin{block}{A Comparison}
         It is a "contest" between $f(n)$ and $n^{\log_b a}$ (the number of leaves in a recursion tree).
    \end{block}
    
    \alert{Case 1}: $f(n) = O(n^{\log_b a - \epsilon})$ for some constant $\epsilon > $.
    

    \begin{description}    
        \item[Explanation:] ``$(n)$ is \alert{polynomially} smaller than $n^{\log_b a}$."
        \item[Intuition:]  the cost is dominated by the leaves
        \item[Solution:] $f(n) = \Theta(n^{\log_b a})$
    \end{description}

    \begin{block}{Note}
        $n^2 \lg n \neq O(n^{2 - \epsilon})$, to be polynomially smaller the exponents are usually different.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example of Case 1}
    \begin{exampleblock}{Example}
        $$T(n) = \underbrace{5}_a T(n/\underbrace{2}_b) + \Theta(n^2)$$
        
        \begin{itemize}[]
            \item Compare  $f(n) = \Theta(n^2)$ to $n^{\log_2 5}$ 
            \item Is $2 < \log_2 5$?
            \item Yes, since $2^2 = 4 \implies \log_2 5 > 2$.\\
            ($\log_2 5 \approx 2.322 $, but no calculators are needed!)
            \item $T(n) = \Theta(n^{\lg 5})$ \alert{by Case 1 of the Masters theorem}.
        \end{itemize}
        
    \end{exampleblock}
    
    \begin{alertblock}{When using the Master method}
        Make sure you indicate the \alert{which case} clearly, so that graders or reviewers can easily spot correct work. 
    \end{alertblock}
\end{frame}
   
\begin{frame} 
    \frametitle{Case 2}
    $$f(n) = \Theta(n^{\log_b a})$$
   \begin{description}    
       \item[Explanation:] $f(n)$ is bounded above and below by $n^{\log_b a}$
       \item[Intuition:] the cost of each level is about the same (asymptotically).  There are $\Theta(\lg n)$ levels in the tree.
       \item[Solution:] $f(n) = \Theta(\underbrace{n^{\log_b a}}_\text{cost per level} \underbrace{\lg n}_\text{height of tree})$
    \end{description}
    
    \begin{example}
        Merge sort!  $T(n) = 2 T(n/2) + \Theta(n)$, $\Theta(n) = \Theta(n^{\log_2 2}) \implies T(n) = \Theta(n \lg n)$\\by \alert{Case 2 of the Master thoerem}.
    \end{example} 
\end{frame}  

\begin{frame}
    \frametitle{Case 3}
    
    $$f(n) = \Omega(n^{\log_b a + \epsilon}) \text{ for some } \epsilon > 0 \text{ and $f$ is regular} $$
    
    \begin{block}{Regularity Condition}
        $f(n)$ satisfies the \alert{regularity condition} if $a f(n/b) \leq c f(n)$ \\ for some $c<1$ and for all $n$ sufficiently large.  
    \end{block}
    
    \begin{description}    
        \item[Explanation:] $f(n)$ dominates $n^{\log_b a}$
        \item[Intuition:] the cost is dominated by the root of the tree. 
        \item[Solution:] $f(n) = \Theta(f(n))$
    \end{description}
    [More on the regularity condition on the next slide]
\end{frame}

\begin{frame}
    \frametitle{What's with that regularity condition?}
    \begin{itemize}
        \item Generally, it is not a problem.
        \item It is always true for $f(n)=n^k$ if $f(n) = \Omega(n^{\log_b a})$,
        
         so don't worry about it if $f$ is a polynomial. 
         
         \item I will show some examples where it does / does not hold.
         \item But first lets talk more about when the theorem does / does not apply.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Notice the gaps....}
    
    There is a gap between case 1 and case 2.  
    \begin{itemize}
        \item Some functions are not $O(n^{\log_b a - \epsilon}$) -- not in case 1
        \item They are, however, $o(n^{\log_b a})$ -- not in case 2.
        \item You are back to guessing and using the substitution method, sorry!
    \end{itemize}
    
    
    There is a gap between case 2 and case 3.
    \begin{itemize}
        \item Some functions are not $\Omega(n^{\log_b a - \epsilon}$) -- not in case 3. 
        \item They are, however, $\omega(n^{\log_b a})$ -- not in case 2.
    \end{itemize}
    
    \begin{alertblock}{A more general Case 3}
        You book has an exercise to prove this, it can be used on homework and exams: \alert{For $k \geq 0$}
        $$f(n) = \Theta(n^{\log_b a})\alert{\lg^k n} \implies T(n) = \Theta(n^{\log_b a}\alert{\lg^{k+1}n})$$
 
    \end{alertblock}

\end{frame}     

\begin{frame}
    \frametitle{Examples}
    \begin{itemize}
       \item $T(n) = 27 T(n/3) + \Theta(n^3)$
       \item[] $n^{\log_b a}=n^3$ vs $n^3 $
       \item[] This is \alert{case 2} $\implies T(n) = \Theta(n^3 \lg  n)$.
       \item $T(n) = 27 T(n/3) + \Theta(n^3\lg n)$
       \item[] $n^{\log_b a}=n^3$ vs $n^3 \lg n$
       \item[] This is the (more general) \alert{case 2} $\implies T(n) = \Theta(n^3 \lg^2  n)$.
       \item $T(n)=5T(n/2) + \Theta(n^3)$
       \item[] $n^{\lg 5}$ vs. $n^3$  
       \item[] $n^3$ dominates, it is regular, \alert{case 3} $\implies \Theta(n^3)$
       
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{More Examples}
    \begin{itemize}
        \item $T(n) = 5 T(n/2) + \Theta(n^2)$
        \item[] $n^{\lg 5}$ vs $n^2 $
        \item[] This is \alert{case 1} $\implies T(n) = \Theta(n^{\lg 5})$.
        \item $T(n) = 27 T(n/3) + \Theta(n^3 / \lg n)$
        \item[] $n^{\lg 5}$ vs $n^3 / \lg n$
        \item[] This is \alert{not} case 2 since $k$ would  not be positive. 
        \item[] We are in a gap between Case 1 and 2 -- \alert{does not apply}
        \item[] 
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Proof (idea) of the Master Theorem}
   \begin{itemize}
       \item I want to give some intuition of how the proof proceeds. 
       \item The actual proof in gory detail is in your book. 
       \item The next slides shows a recursion tree that is used to derive the cost for general $a$ and $b$ and $f(n)$ -- this is Lemma 4.2 in the CLRS book.
       \item It will help to recall that $a^{\log_b n} = n^{\log b a}$, which was one of the logarithmic identities from CLRS-3.  
   \end{itemize} 
   \begin{center}
\includegraphics[width=0.2\linewidth]{handwaving}
\reflectbox{\includegraphics[width=0.2\linewidth]{handwaving}}
\end{center}

   
\end{frame}



\begin{frame}
    \frametitle{Proof (idea) of the Master Theorem}
 
    \begin{center}
\includegraphics[width=1.0\linewidth]{"IM/Chapter 4/Fig-4-7"}
\end{center}

\end{frame}


\begin{frame}
    \frametitle{Proof (idea) of the Master Theorem}
    \begin{itemize}
        \item Notice that the total cost is a sum if the cost of each level (Lemma 4.2)
        \item The leaf nodes are unaffected by $f(n)$
        \item The leaf nodes are separated out -- their cost is $\Theta(n^{\log_b a})$ because that is how many there are.
        \item  The cost of the other levels is described by Lemma 4.3 (next slide, only for exact powers of $b$)
        
    \end{itemize} 
\end{frame}

\begin{frame}
    \frametitle{Proof for exact powers of $b$}
    \begin{theorem}[Lemma 4.3]
      Let $a \geq 1$ and $b > 1$ be constants and $f(n/) \geq 0$ defined for \alert{exact powers} of $b$. A function $g(n)$ defined for \alert{exact powers} of $b$ by:
      $$g(n) = \sum_{j=0}^{\log_b n -1} a^j f(n/b^j)$$
      has the following asymptotic bounds for $\epsilon>0$, and $c<1$, and for sufficiently large $n$:
      \begin{enumerate}
          \item[1.] $f(n) = O(n^{\log_b a -\epsilon}) \implies g(n) = O(n^{\log_b a}),$
          \item[2.] $f(n) = \Theta(n^{\log_b a}) \implies g(n ) = \Theta(n^{\log_b a} \lg n),$
          \item[3.] $a f(n/b) \leq c f(n) \implies g(n) = \Theta(f(n)).$
      \end{enumerate}
    \end{theorem}
    
    
%     \begin{itemize}
%         \item Dominated by the cost of the leaf nodes.
%         \item Evenly distributed among the levels, in which case $f(n)=\Theta(n^{\log_b a})$ since the leaf nodes are one of the levels. 
%         \item Part of a converging geometric series if $af(n/b) < cf(n)$ and $c<1$. If the series converges then the summation is $\Theta(f(n))$.
%      \end{itemize}
\end{frame}

\begin{frame}{Why use Lemma 4.3?}
	\begin{itemize}
		\item \alert{Case 1} allows us to prove we are $\Theta(n^{\log_b a})$ and not just $\Omega(n^{\log_b a})$, which is in another lemma and clear from the tree.
		\item \alert{Case 2 \& 3} complete the Master theorem
	\end{itemize}
\end{frame}
   
\begin{frame}
    \frametitle{Proof for exact powers}
    \framesubtitle{Case 1}
    \begin{align*}
    g(n) &= \sum_{j=0}^{\log_b n - 1} a^j f(n/b^j) &  \text{by definition} \\
         &=  \sum_{j=0}^{\log_b n - 1} a^j O( (n/b^j)^{\log_b a - \epsilon} ) & \text{since we are in case 1} \\
         &= O(\sum_{j=0}^{\log_b n - 1} a^j (n/b^j)^{\log_b a - \epsilon} ) & \text{distributing $O$}\\         
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Proof for exact powers}
    \framesubtitle{Case 1}
    \begin{align*}
         \sum_{j=0}^{\log_b n - 1} a^j (\frac{n}{b^j})^{\log_b a - \epsilon} 
%    &=  \sum_{j=0}^{\log_b n - 1} a^j n^{\log_b a - \epsilon}(\frac{1}{b^j})^{\log_b a - \epsilon}  & \text{factoring out}\\
     &=  n^{\log_b a - \epsilon} \sum_{j=0}^{\log_b n - 1} a^j(\frac{1}{b^j})^{\log_b a - \epsilon} &\\
     &=  n^{\log_b a - \epsilon} \sum_{j=0}^{\log_b n - 1} a^j\left(\frac{1}{b^{\log_b a-\epsilon}}\right)^{j} &\\
     &=  n^{\log_b a - \epsilon} \sum_{j=0}^{\log_b n - 1} \left(\frac{a b^\epsilon}{{b^{\log_b a}}}\right)^j & \text{log. ident.}\\
     &=  n^{\log_b a - \epsilon} \sum_{j=0}^{\log_b n - 1} \left(\frac{\cancel{a}b^\epsilon}{\cancel{a}}\right)^j & \\
    \end{align*}
    
\end{frame}
   
\begin{frame}[shrink=20]
    \frametitle{Proof for exact powers}
    \framesubtitle{Case 1}
        \begin{align*}
     n^{\log_b a - \epsilon} \sum_{j=0}^{\log_b n - 1} (b^{\epsilon})^j &=   n^{\log_b a - \epsilon}\left(\frac{b^{\epsilon \log_b n} - 1}{b^\epsilon -1}\right) & \text{geometric series (A.5)}\\
      &=   n^{\log_b a - \epsilon}\left(\frac{[b^{\log_b n}]^\epsilon - 1}{b^\epsilon -1}\right) & \\
      &=   n^{\log_b a - \epsilon}\left(\frac{n^\epsilon - 1}{b^\epsilon -1}\right) & \text{log. identity}\\
      &= n^{\log_b a - \epsilon} O(n^\epsilon) &\text{since $b$ is constant}\\
      &= O(n^{\log_b a}) & \text{distribute $O$} \\
      &\qed
    \end{align*}
    
 That proved an upper bound for 1 for exact powers of 2, all it needed was a lot of algebra and careful attention to logarithmic identities, and A.5. 
    
 \end{frame}
   

\end{document}
