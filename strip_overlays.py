
#USAGE:   x | python strip_overlays.py > out.tex
import sys
import re
for line in sys.stdin:
    line = re.sub(r'\<[\+\-0-9]+(,[0-9])*\>', '', line)
    line = re.sub(r'\\pause', '', line)
    line = re.sub(r'\\visible', '', line)
    line = re.sub(r'\\overlay', '', line)
    print line,

